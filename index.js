const fs = require('fs');
const os = require("os");

const credentials = fs.readFileSync('credentials.txt', 'utf-8').split('\n');

const TwitchClient = new (require('./twitchClient'))(credentials[0], [credentials[1]]);

const lookup = ['title', 'thumbnail_url', 'game_id'];

fs.writeFile('output.txt', '', function () {});

fs.readFile('channels.txt', 'utf-8', function (err, data) {
    if (err) throw err;
    const channels = data.split("\n");

    TwitchClient.setAuthentication().then(() => {
        channels.forEach(channel => {
            TwitchClient.searchChannels(channel).then(({data}) => {
                const item = data.data[0];

                //Add channel name and newline
                let writeData = `${channel}`;
                writeData += os.EOL;

                //Add all specific values to newlines
                lookup.forEach(lookupValue => {
                    writeData += '    ';
                    writeData += `${lookupValue} : ${item[lookupValue]}`;
                    writeData += os.EOL;
                });

                //Write data to file
                fs.appendFile('output.txt', `${writeData}`, function () {
                    console.log(`added ${channel} to output`);
                });


            }).catch(err => {
                console.log(err);
            });
        });
    });
});