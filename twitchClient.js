const axios = require('axios');


module.exports = class TwitchClient {
    constructor(clientId, clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.access_token = null;
        axios.defaults.headers.common['Client-ID'] = this.clientId;
    }

    setAuthentication() {
        return new Promise(((resolve, reject) => {
            axios.post(`https://id.twitch.tv/oauth2/token?grant_type=client_credentials&client_id=${this.clientId}&client_secret=${this.clientSecret}`).then(({data}) => {
                this.access_token = data.access_token;
                axios.defaults.headers.common['Authorization'] = `Bearer ${this.access_token}`;
                resolve();
            }).catch(err => {
                console.log(err);
            });
        }));
    }
    searchChannels(user) {
        return axios.get(`https://api.twitch.tv/helix/search/channels?query=${user}`);
    }
};